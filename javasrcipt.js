//HAMBURGER MENU
$(document).ready(function(){
    $('#hamburger').click(function(){
        $(this).toggleClass('hamburger');
    });
});

var action = 1;

$("#hamburger").on("click", viewSomething);

function viewSomething() {
    if ( action == 1 ) {
        $("#menu").removeClass("main-menu");
        $("#menu").addClass("hamburger-menu");
        action = 2;
    } else {
        $("#menu").removeClass("hamburger-menu");
        $("#menu").addClass("main-menu");
        action = 1;
    }
}

//SCROLLING
function scrollTo(buttonID, sectionId){
    $(buttonID).click(function() {
        $('html, body').animate({
            scrollTop: $(sectionId).offset().top
        }, 1000);
    });
}

scrollTo("#home-button", "#home");
scrollTo('#services-button', '#services');
scrollTo('#features-button', '#features');
scrollTo('#portfolio-button', '#portfolio');
scrollTo('#contact-button', '#contact');
